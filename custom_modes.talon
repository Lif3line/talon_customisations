not mode: sleep
-
^enable game mode$:
    mode.disable("sleep")
    mode.disable("command")
    mode.disable("dictation")
    mode.enable("user.game")

^disable game mode$:
    mode.enable("command")
    mode.disable("user.game")