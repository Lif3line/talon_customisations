-
find on page: key(ctrl-f)

(duplicate|dupe) line:
    key(home)
    key(ctrl-shift-d)

(duplicate|dupe) that <number_small> times:
    key(ctrl-c)
    key(right)
    key("ctrl-v:{number_small}")

raze line:
    edit.select_line()
    key(delete:2)

test click:
    user.mouse_click_at(0, 1545, 69, 50000)

