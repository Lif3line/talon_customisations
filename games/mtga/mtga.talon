title: MTGA
title: Untapped.gg
mode: user.game
mode: command
-
settings():
    key_hold = 32

highlight cards|show cards:
    user.mtga_hide_highlighted_cards()
    user.mtga_highlight_cards()

keep highlighting cards:
    user.mtga_highlight_continually()

stop highlighting cards:
    user.mtga_stop_highlighting_continually()

stop highlight|stop highlights|remove highlights:
    user.mtga_hide_highlighted_cards()

play <number_small>:
    user.mtga_play_a_card(number_small)

fast <number_small>:
    user.mtga_play_a_card(number_small, 1)

select card <number_small>:
    user.mtga_select_card(number_small, 0)

select fast <number_small>:
    user.mtga_select_card(number_small, 1)

play game:
    user.mtga_press_btn("orange")

keep hand|keep <number_small>:
    user.mtga_press_btn("orange", 1)

mulligan:
    user.mtga_press_btn("blue", 1)

done:
    user.mtga_press_btn("orange", 2)

(pass|resolve|resolves|next|[auto] pay|my turn|take action):
    key(space)

all attack|confirm attack|confirm block|no blocks:
    key(space)

no attacks:
    user.mtga_press_btn("blue")

resolve all|decline:
    mouse_move(2365, 1174)
    mouse_click(0)

(back side|foretell):
    mouse_move(1568, 644)
    mouse_click(0)

cancel:
    user.mtga_press_btn("blue,orange")

front side:
    mouse_move(985, 641)
    mouse_click(0)

enters tapped:
    mouse_move(1521, 1161)
    mouse_click(0)

cancel attacks:
    mouse_move(2370, 1178)
    mouse_click(0)

stop at end of [your] turn:
    mouse_move(1458, 165)
    mouse_click(0)

stop at start of [your] turn:
    mouse_move(1097, 169)
    mouse_click(0)

stop at start of my turn:
    mouse_move(2283, 1193)
    mouse_click(0)

stop at end of my turn:
    mouse_move(2458, 1200)
    mouse_click(0)

pay cost:
    mouse_move(1048, 1173)
    mouse_click(0)

keep passing:
    key(enter)

pass turn|end turn:
    key(shift-enter)

concede the game:
    mouse_move(2507, 43)
    mouse_click(0)
    sleep(500ms)
    mouse_move(1281, 853)
    mouse_drag(0)
    sleep(100ms)
    mouse_release(0)

emote oops:
    mouse_move(1280, 1125)
    mouse_click(1)
    sleep(250ms)
    mouse_move(1531, 1007)
    mouse_drag(0)
    sleep(100ms)
    mouse_release(0)

good game:
    mouse_move(1280, 1125)
    mouse_click(1)
    sleep(250ms)
    mouse_move(1577, 1092)
    mouse_drag(0)
    sleep(100ms)
    mouse_release(0)

emote hello:
    mouse_move(1280, 1125)
    mouse_click(1)
    sleep(250ms)
    mouse_move(1279, 916)
    mouse_drag(0)
    sleep(100ms)
    mouse_release(0)

emote nice:
    mouse_move(1280, 1125)
    mouse_click(1)
    sleep(250ms)
    mouse_move(975, 1098)
    mouse_drag(0)
    sleep(100ms)
    mouse_release(0)

take back:
    key(Z)

full control:
    key(ctrl)
