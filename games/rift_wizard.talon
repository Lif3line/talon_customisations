title: Rift Wizard
mode: command
-
character sheet:
    key(C)

learn spells:
    key(S)

learn skill:
    key(K)

north:
    key(keypad_8)
    sleep(200ms)

north east:
    key(keypad_9)
    sleep(200ms)

east:
    key(keypad_6)
    sleep(200ms)

south east:
    key(keypad_3)
    sleep(200ms)

south:
    key(keypad_2)
    sleep(200ms)

south west:
    key(keypad_1)
    sleep(200ms)

west:
    key(keypad_4)
    sleep(200ms)

north west:
    key(keypad_7)
    sleep(200ms)

spell <number_small>:
    key("{number_small}")

cast <number_small>:
    key("{number_small}")
    sleep(100ms)
    mouse_click(0)    

item <number_small>:
    key("alt-{number_small}:down")
    sleep(100ms)
    key("alt-{number_small}:up")

use item <number_small>:
    key("alt-{number_small}:down")
    sleep(100ms)
    key("alt-{number_small}:up")
    sleep(100ms)    
    mouse_click(0)  

wait:
    key(keypad_5)
    sleep(200ms)

cancel that:
    key(esc)

show threatened:
    key(t)

auto loot:
    key(a)

