from talon import Context, Module, actions

mod = Module()

modes = {
    "game": "Disable other commands to improve recognition and reduce chance of accidental activations"
}

for key, value in modes.items():
    mod.mode(key, value)
